'use strict'

function addUserFetch(userInfo) {
  let options = {
    method: "POST",
    headers: {
      "Content-Type": "application/json"
    },
    body: JSON.stringify(userInfo)
  }
  return fetch('/users', options)
    .then(response => {
      if (response.ok) return response.json().then(user => user.id)
      else return Promise.reject(`error: ${response.statusText}, status: ${response.status}`)
    })
}

function getUserFetch(id) {
  return fetch(`/users/${id}`)
    .then(response => {
      if (response.ok) return response.json()
      else return Promise.reject(`error: ${response.statusText}, status: ${response.status}`)
    })
}

function addUserXHR(userInfo) {
  const xhr = new XMLHttpRequest()
  xhr.open('POST', '/users')
  xhr.setRequestHeader('Content-Type', 'application/json')
  xhr.send(JSON.stringify(userInfo))
  return new Promise((resolve, reject) => {
    xhr.onload = () => {
      if (xhr.status > 399) reject(`${xhr.status} ${xhr.statusText}`)
      else resolve(JSON.parse(xhr.responseText).id)
    }
  })
}

function getUserXHR(id) {
  const xhr = new XMLHttpRequest()
  xhr.open('GET', `/users/${id}`)
  xhr.send()
  return new Promise((resolve, reject) => {
    xhr.onload = () => {
      if (xhr.status > 399) reject(`${xhr.status} ${xhr.statusText}`)
      else resolve(JSON.parse(xhr.responseText))
    }
  })
}

function checkWork() {  
  addUserXHR({ name: "Alice", lastname: "FetchAPI" })
    .then(userId => {
      console.log(userId);
      console.log(`Был добавлен пользователь с userId ${userId}`);
      return getUserXHR(userId);
    })
    .then(userInfo => {
      console.log(`Был получен пользователь:`, userInfo);
    })
    .catch(error => {
      console.error(error);
    });
}

checkWork()